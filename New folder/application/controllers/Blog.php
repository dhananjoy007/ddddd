<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Blog extends CI_Controller {
    
    
    function __contruct(){
        
        parent:: __construct();
        $this->load->model('Blog_m');
        $this->load->database();
        
        
    }
    
    
    public function add()
    { 
        $this->load->model('Blog_m');
        $this->load->view('layout/header');
		$this->load->view('blog/add');
        $this->load->view('layout/footer');
    }
    
	function index()
	{
        $this->load->model('Blog_m');
        $data['blogs'] = $this->Blog_m->getBlog(); 
        $this->load->view('layout/header');
		$this->load->view('blog/index', $data);
        $this->load->view('layout/footer');
         
	}
    
   function submit(){
        $this->load->model('Blog_m');
        $result = $this->Blog_m->submit();
        if($result){
            $this->session->set_flashdata('success_msg', 'Successfully uploaded data.');
        }else{
            $this->session->set_flashdata('error_msg', 'Fail to upload data.');
        }
        redirect(base_url('blog/index'));
   }

   public function edit($id){
       $this->load->model('Blog_m');
        $data['blog']  = $this->Blog_m->getBlogById($id);
       $this->load->view('layout/header');
       $this->load->view('blog/edit', $data);
       $this->load->view('layout/footer');
   }

   public function update(){
        $this->load->model('Blog_m');
        $result = $this->Blog_m->update();
       if($result){
           $this->session->set_flashdata('success_msg', 'Successfully updated record.');
       }else{
           $this->session->set_flashdata('error_msg', 'Fail to update record.');
       }
       redirect(base_url('blog/index'));
   }

   public function delete($id){
       $this->load->model('Blog_m');
       $result = $this->Blog_m->delete($id);
       if($result){
           $this->session->set_flashdata('success_msg', 'Successfully deleted record.');
       }else{
           $this->session->set_flashdata('error_msg', 'Fail to delete record.');
       }
       redirect(base_url('blog/index'));
   }

    
}
