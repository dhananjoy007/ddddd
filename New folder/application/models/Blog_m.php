<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Blog_m extends CI_Model{
        
    function getBlog(){
            $this->db->order_by('create', 'desc');
            $query = $this->db->get('blog-detail');
            if($query->num_rows()>0){
                return $query->result(); 
            }   else{
                
                return false;
            }
            
        }
        
     public function submit(){
        
        $field = array(
            'title'=>$this->input->post('txt_title'),
            'description'=>$this->input->post('txt_description'),
            'create'=>date('Y-m-d H:i:s')
            );
        $this->db->insert('blog-detail', $field);
        if($this->db->affected_rows() > 0){
            return true;
        } else{
            return false;
        }
    }

    public function getBlogById($id){
         $this->db->where('id', $id);
         $query = $this->db->get('blog-detail');
         if($query->num_rows() > 0){
             return $query->row();
         } else{
             return false;
         }
    }

    public function update(){
        $id = $this->input->post('txt_hidden');
        $field = array(
            'title'=>$this->input->post('txt_title'),
            'description'=>$this->input->post('txt_description'),
            'updated'=>date('Y-m-d H:i:s')
        );
        $this->db->where('id', $id);
        $this->db->update('blog-detail', $field);
        echo $this->db->last_query();extit;
        if($this->db->affected_rows() > 0){
            return true;
        } else{
            return false;
        }

    }

    public function delete($id){
        $this->db->where('id', $id);
        $this->db->delete('blog-detail');
        if($this->db->affected_rows() > 0){
            return true;
        } else{
            return false;
        }
        
    }
        
        
    }



?>